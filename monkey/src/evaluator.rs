use std::collections::HashMap;
use std::iter;

use once_cell::sync::Lazy;

use crate::ast::ArrayExpression;
use crate::ast::BlockStatement;
use crate::ast::BooleanExpression;
use crate::ast::CallExpression;
use crate::ast::Expression;
use crate::ast::ExpressionStatement;
use crate::ast::FunctionExpression;
use crate::ast::IdentifierExpression;
use crate::ast::IfExpression;
use crate::ast::IndexExpression;
use crate::ast::InfixExpression;
use crate::ast::IntegerLiteralExpression;
use crate::ast::LetStatement;
use crate::ast::Node;
use crate::ast::PrefixExpression;
use crate::ast::Program;
use crate::ast::ReturnStatement;
use crate::ast::Statement;
use crate::ast::StringExpression;
use crate::object::ArrayObject;
use crate::object::BooleanObject;
use crate::object::BuiltinObject;
use crate::object::Environment;
use crate::object::ErrorObject;
use crate::object::FunctionObject;
use crate::object::IntegerObject;
use crate::object::NullObject;
use crate::object::Object;
use crate::object::ObjectT;
use crate::object::ReturnValueObject;
use crate::object::StringObject;

#[cfg(test)]
mod tests;

fn monkey_len(args: Vec<Object>) -> Object {
    if args.len() != 1 {
        return ErrorObject {
            message: format!("wrong number of arguments. got={}, want=1", args.len()),
        }
        .into();
    }

    match &args[0] {
        Object::String(s) => IntegerObject {
            value: s.value.len() as i64,
        }
        .into(),
        Object::Array(a) => IntegerObject {
            value: a.elements.len() as i64,
        }
        .into(),
        a => ErrorObject {
            message: format!("argument to `len` not supported, got {}", a.ty()),
        }
        .into(),
    }
}

fn monkey_first(args: Vec<Object>) -> Object {
    if args.len() != 1 {
        return ErrorObject {
            message: format!("wrong number of arguments. got={}, want=1", args.len()),
        }
        .into();
    }

    match &args[0] {
        Object::Array(ArrayObject { elements }) => {
            if !elements.is_empty() {
                elements[0].clone()
            } else {
                NullObject.into()
            }
        }
        _ => ErrorObject {
            message: format!("argument to `first` must be ARRAY, got {}", args[0].ty()),
        }
        .into(),
    }
}

fn monkey_last(args: Vec<Object>) -> Object {
    if args.len() != 1 {
        return ErrorObject {
            message: format!("wrong number of arguments. got={}, want=1", args.len()),
        }
        .into();
    }

    match &args[0] {
        Object::Array(ArrayObject { elements }) => {
            if !elements.is_empty() {
                elements.last().cloned().unwrap()
            } else {
                NullObject.into()
            }
        }
        _ => ErrorObject {
            message: format!("argument to `last` must be ARRAY, got {}", args[0].ty()),
        }
        .into(),
    }
}

fn monkey_rest(args: Vec<Object>) -> Object {
    if args.len() != 1 {
        return ErrorObject {
            message: format!("wrong number of arguments. got={}, want=1", args.len()),
        }
        .into();
    }

    match &args[0] {
        Object::Array(ArrayObject { elements }) => {
            if !elements.is_empty() {
                ArrayObject {
                    elements: elements.iter().cloned().skip(1).collect(),
                }
                .into()
            } else {
                NullObject.into()
            }
        }
        _ => ErrorObject {
            message: format!("argument to `rest` must be ARRAY, got {}", args[0].ty()),
        }
        .into(),
    }
}

fn monkey_push(args: Vec<Object>) -> Object {
    if args.len() != 2 {
        return ErrorObject {
            message: format!("wrong number of arguments. got={}, want=2", args.len()),
        }
        .into();
    }

    match &args[0] {
        Object::Array(ArrayObject { elements }) => ArrayObject {
            elements: elements
                .iter()
                .cloned()
                .chain(iter::once(args[1].clone()))
                .collect(),
        }
        .into(),
        _ => ErrorObject {
            message: format!("argument to `rest` must be ARRAY, got {}", args[0].ty()),
        }
        .into(),
    }
}

static BUILTINS: once_cell::sync::Lazy<HashMap<&'static str, Object>> = Lazy::new(|| {
    let mut builtins = HashMap::new();
    builtins.insert(
        "len",
        BuiltinObject {
            function: monkey_len,
        }
        .into(),
    );
    builtins.insert(
        "first",
        BuiltinObject {
            function: monkey_first,
        }
        .into(),
    );
    builtins.insert(
        "last",
        BuiltinObject {
            function: monkey_last,
        }
        .into(),
    );
    builtins.insert(
        "rest",
        BuiltinObject {
            function: monkey_rest,
        }
        .into(),
    );
    builtins.insert(
        "push",
        BuiltinObject {
            function: monkey_push,
        }
        .into(),
    );
    builtins
});

pub fn eval(node: Node, env: &mut Environment) -> Object {
    match node {
        Node::Expression(Expression::IntegerLiteral(IntegerLiteralExpression {
            value, ..
        })) => IntegerObject { value }.into(),
        Node::Expression(Expression::String(StringExpression { value, .. })) => {
            StringObject { value }.into()
        }
        Node::Expression(Expression::Boolean(BooleanExpression { value, .. })) => {
            BooleanObject { value }.into()
        }
        Node::Expression(Expression::Prefix(PrefixExpression {
            operator, right, ..
        })) => eval_prefix_expression(operator, *right, env),
        Node::Expression(Expression::Infix(InfixExpression {
            left,
            operator,
            right,
            ..
        })) => eval_infix_expression(operator, *left, *right, env),
        Node::Expression(Expression::If(IfExpression {
            condition,
            consequence,
            alternative,
            ..
        })) => eval_if_expression(*condition, *consequence, alternative, env),
        Node::Expression(Expression::Identifier(IdentifierExpression { value, .. })) => {
            match env
                .get(&value)
                .or_else(|| BUILTINS.get(value.as_str()).cloned())
            {
                Some(val) => val,
                None => {
                    return ErrorObject {
                        message: format!("identifier not found: {}", value),
                    }
                    .into()
                }
            }
        }
        Node::Expression(Expression::Function(FunctionExpression {
            parameters, body, ..
        })) => {
            let body = *body;
            FunctionObject {
                parameters,
                body,
                env: env.clone(),
            }
            .into()
        }
        Node::Expression(Expression::Call(CallExpression {
            function,
            arguments,
            ..
        })) => {
            let function = eval((*function).into(), env);
            if function.is_error() {
                return function;
            }
            let args = eval_expressions(arguments, env);
            if args.len() == 1 && args[0].is_error() {
                return args[0].clone();
            }
            apply_function(function, args)
        }
        Node::Expression(Expression::Array(ArrayExpression { elements, .. })) => {
            let elements = eval_expressions(elements, env);
            if elements.len() == 1 && elements[0].is_error() {
                return elements[0].clone();
            }
            ArrayObject { elements }.into()
        }
        Node::Expression(Expression::Index(IndexExpression { left, index, .. })) => {
            eval_index_expression(*left, *index, env)
        }
        Node::Program(program) => eval_program(program, env),
        Node::Statement(Statement::Expression(ExpressionStatement { expression, .. })) => {
            eval(expression.into(), env)
        }
        Node::Statement(Statement::Block(block_statement)) => {
            eval_block_statement(block_statement, env)
        }
        Node::Statement(Statement::Return(ReturnStatement { return_value, .. })) => {
            let value = eval(return_value.into(), env);
            if value.is_error() {
                return value;
            }
            let value = Box::new(value);
            ReturnValueObject { value }.into()
        }
        Node::Statement(Statement::Let(LetStatement { name, value, .. })) => {
            eval_let_statement(name, value, env)
        }
    }
}

fn eval_index_expression(left: Expression, index: Expression, env: &mut Environment) -> Object {
    let left = eval(left.into(), env);
    if left.is_error() {
        return left;
    }
    let index = eval(index.into(), env);
    if index.is_error() {
        return index;
    }
    eval_index(left, index)
}

fn eval_index(left: Object, index: Object) -> Object {
    if left.is_array() && index.is_integer() {
        eval_array_index_expression(left.as_array().unwrap(), index.as_integer().unwrap())
    } else {
        ErrorObject {
            message: format!("index operator not supported: {}", left.ty()),
        }
        .into()
    }
}

fn eval_array_index_expression(left: &ArrayObject, index: &IntegerObject) -> Object {
    let max = left.elements.len() - 1;
    if index.value < 0 || index.value > max as i64 {
        return NullObject.into();
    }

    left.elements[index.value as usize].clone()
}

fn apply_function(function: Object, args: Vec<Object>) -> Object {
    match function {
        Object::Function(function) => {
            let mut extended_env = extend_function_env(function.parameters, function.env, args);
            let evaluated = eval(Statement::Block(function.body).into(), &mut extended_env);
            unwrap_return_value(evaluated)
        }
        Object::Builtin(b) => {
            let func = b.function;
            func(args)
        }
        _ => {
            return ErrorObject {
                message: format!("not a function: {}", function),
            }
            .into()
        }
    }
}

fn unwrap_return_value(evaluated: Object) -> Object {
    match evaluated {
        Object::ReturnValue(ReturnValueObject { value }) => *value,
        other => other,
    }
}

fn extend_function_env(
    params: Vec<IdentifierExpression>,
    env: Environment,
    args: Vec<Object>,
) -> Environment {
    let mut env = Environment::new_enclosed(Box::new(env));

    for (arg, param) in args.into_iter().zip(params.into_iter()) {
        env.set(param.value, arg);
    }

    env
}

fn eval_expressions(exps: Vec<Expression>, env: &mut Environment) -> Vec<Object> {
    let mut result = Vec::new();

    for exp in exps {
        let evaluated = eval(exp.into(), env);
        if evaluated.is_error() {
            return vec![evaluated];
        }

        result.push(evaluated);
    }

    result
}

fn eval_let_statement(
    name: IdentifierExpression,
    value: Expression,
    env: &mut Environment,
) -> Object {
    let value = eval(value.into(), env);
    if value.is_error() {
        return value;
    }
    env.set(name.value, value)
}

fn eval_block_statement(block_statement: BlockStatement, env: &mut Environment) -> Object {
    let mut result: Object = NullObject.into();

    for statement in block_statement.statements {
        result = eval(statement.into(), env);

        if result.is_error() || result.is_return_value() {
            return result;
        }
    }

    result
}

fn eval_if_expression(
    condition: Expression,
    consequence: BlockStatement,
    alternative: Option<Box<BlockStatement>>,
    env: &mut Environment,
) -> Object {
    let condition = eval(condition.into(), env);
    if condition.is_error() {
        return condition;
    }

    if is_truthy(condition) {
        eval(Statement::Block(consequence).into(), env)
    } else if let Some(alternative) = alternative {
        eval(Statement::Block(*alternative).into(), env)
    } else {
        NullObject.into()
    }
}

fn is_truthy(condition: Object) -> bool {
    !matches!(
        condition,
        Object::Null(NullObject) | Object::Boolean(BooleanObject { value: false })
    )
}

fn eval_infix_expression(
    operator: String,
    left: Expression,
    right: Expression,
    env: &mut Environment,
) -> Object {
    let left = eval(left.into(), env);
    if left.is_error() {
        return left;
    }
    let right = eval(right.into(), env);
    if right.is_error() {
        return right;
    }

    if left.is_integer() && right.is_integer() {
        return eval_integer_infix_expression(
            operator,
            left.as_integer().unwrap(),
            right.as_integer().unwrap(),
        );
    }

    if left.is_string() && right.is_string() {
        return eval_string_infix_expression(
            operator,
            left.as_string().unwrap(),
            right.as_string().unwrap(),
        );
    }

    if operator == "==" {
        return BooleanObject {
            value: left == right,
        }
        .into();
    }
    if operator == "!=" {
        return BooleanObject {
            value: left != right,
        }
        .into();
    }

    if left.ty() != right.ty() {
        return ErrorObject {
            message: format!("type mismatch: {} {} {}", left.ty(), operator, right.ty()),
        }
        .into();
    }

    ErrorObject {
        message: format!(
            "unknown operator: {} {} {}",
            left.ty(),
            operator,
            right.ty()
        ),
    }
    .into()
}

fn eval_string_infix_expression(
    operator: String,
    left: &StringObject,
    right: &StringObject,
) -> Object {
    match operator.as_str() {
        "+" => {
            let mut result = String::from(&left.value);
            result.push_str(&right.value);
            StringObject { value: result }.into()
        }
        _ => ErrorObject {
            message: format!(
                "unknown operator: {} {} {}",
                left.ty(),
                operator,
                right.ty()
            ),
        }
        .into(),
    }
}

fn eval_integer_infix_expression(
    operator: String,
    left: &IntegerObject,
    right: &IntegerObject,
) -> Object {
    let left_val = left.value;
    let right_val = right.value;

    match operator.as_str() {
        "+" => IntegerObject {
            value: left_val + right_val,
        }
        .into(),
        "-" => IntegerObject {
            value: left_val - right_val,
        }
        .into(),
        "*" => IntegerObject {
            value: left_val * right_val,
        }
        .into(),
        "/" => IntegerObject {
            value: left_val / right_val,
        }
        .into(),
        "<" => BooleanObject {
            value: left_val < right_val,
        }
        .into(),
        ">" => BooleanObject {
            value: left_val > right_val,
        }
        .into(),
        "==" => BooleanObject {
            value: left_val == right_val,
        }
        .into(),
        "!=" => BooleanObject {
            value: left_val != right_val,
        }
        .into(),
        _ => ErrorObject {
            message: format!(
                "unknown operator: {} {} {}",
                left.ty(),
                operator,
                right.ty()
            ),
        }
        .into(),
    }
}

fn eval_prefix_expression(operator: String, right: Expression, env: &mut Environment) -> Object {
    let right = eval(right.into(), env);
    if right.is_error() {
        return right;
    }
    match operator.as_str() {
        "!" => eval_bang_operator_expression(right),
        "-" => eval_minus_prefix_operator_expression(right),
        _ => ErrorObject {
            message: format!("unknown operator: {}{}", operator, right.ty()),
        }
        .into(),
    }
}

fn eval_minus_prefix_operator_expression(right: Object) -> Object {
    let value = match right {
        Object::Integer(i) => i.value,
        _ => {
            return ErrorObject {
                message: format!("unknown operator: -{}", right.ty()),
            }
            .into()
        }
    };
    IntegerObject { value: -value }.into()
}

fn eval_bang_operator_expression(right: Object) -> Object {
    match right {
        Object::Boolean(BooleanObject { value: false }) | Object::Null(NullObject) => {
            BooleanObject { value: true }.into()
        }
        _ => BooleanObject { value: false }.into(),
    }
}

fn eval_program(program: Program, env: &mut Environment) -> Object {
    let mut result: Object = NullObject.into();

    for statement in program.statements {
        result = eval(statement.into(), env);

        if let Object::ReturnValue(rv) = &result {
            return *rv.value.clone();
        }
        if result.is_error() {
            return result;
        }
    }

    result
}
