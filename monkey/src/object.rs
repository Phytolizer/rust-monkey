use std::collections::HashMap;
use std::fmt::Display;

use enum_dispatch::enum_dispatch;

use crate::ast::BlockStatement;
use crate::ast::IdentifierExpression;

#[derive(Debug, Clone, Copy, PartialEq, Eq)]
pub struct ObjectType(&'static str);

impl Display for ObjectType {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", self.0)
    }
}

pub const BOOLEAN_OBJ: ObjectType = ObjectType("BOOLEAN");
pub const INTEGER_OBJ: ObjectType = ObjectType("INTEGER");
pub const NULL_OBJ: ObjectType = ObjectType("NULL");
pub const RETURN_VALUE_OBJ: ObjectType = ObjectType("RETURN_VALUE");
pub const ERROR_OBJ: ObjectType = ObjectType("ERROR");
pub const FUNCTION_OBJ: ObjectType = ObjectType("FUNCTION");
pub const STRING_OBJ: ObjectType = ObjectType("STRING");
pub const BUILTIN_OBJ: ObjectType = ObjectType("BUILTIN");
pub const ARRAY_OBJ: ObjectType = ObjectType("ARRAY");

#[enum_dispatch]
pub trait ObjectT: Display {
    fn ty(&self) -> ObjectType;
    fn inspect(&self) -> String {
        self.to_string()
    }
}

#[enum_dispatch(ObjectT)]
#[derive(Debug, Clone, PartialEq, Eq)]
pub enum Object {
    Boolean(BooleanObject),
    Integer(IntegerObject),
    ReturnValue(ReturnValueObject),
    Null(NullObject),
    Error(ErrorObject),
    Function(FunctionObject),
    String(StringObject),
    Builtin(BuiltinObject),
    Array(ArrayObject),
}

impl Object {
    /// Returns `true` if the object is [`Boolean`].
    ///
    /// [`Boolean`]: Object::Boolean
    pub fn is_boolean(&self) -> bool {
        matches!(self, Self::Boolean(..))
    }

    /// Returns `true` if the object is [`Integer`].
    ///
    /// [`Integer`]: Object::Integer
    pub fn is_integer(&self) -> bool {
        matches!(self, Self::Integer(..))
    }

    /// Returns `true` if the object is [`Null`].
    ///
    /// [`Null`]: Object::Null
    pub fn is_null(&self) -> bool {
        matches!(self, Self::Null(..))
    }

    pub fn as_boolean(&self) -> Option<&BooleanObject> {
        if let Self::Boolean(v) = self {
            Some(v)
        } else {
            None
        }
    }

    pub fn as_integer(&self) -> Option<&IntegerObject> {
        if let Self::Integer(v) = self {
            Some(v)
        } else {
            None
        }
    }

    pub fn as_null(&self) -> Option<&NullObject> {
        if let Self::Null(v) = self {
            Some(v)
        } else {
            None
        }
    }

    /// Returns `true` if the object is [`ReturnValue`].
    ///
    /// [`ReturnValue`]: Object::ReturnValue
    pub fn is_return_value(&self) -> bool {
        matches!(self, Self::ReturnValue(..))
    }

    pub fn as_return_value(&self) -> Option<&ReturnValueObject> {
        if let Self::ReturnValue(v) = self {
            Some(v)
        } else {
            None
        }
    }

    /// Returns `true` if the object is [`Error`].
    ///
    /// [`Error`]: Object::Error
    pub fn is_error(&self) -> bool {
        matches!(self, Self::Error(..))
    }

    pub fn as_error(&self) -> Option<&ErrorObject> {
        if let Self::Error(v) = self {
            Some(v)
        } else {
            None
        }
    }

    /// Returns `true` if the object is [`Function`].
    ///
    /// [`Function`]: Object::Function
    pub fn is_function(&self) -> bool {
        matches!(self, Self::Function(..))
    }

    pub fn as_function(&self) -> Option<&FunctionObject> {
        if let Self::Function(v) = self {
            Some(v)
        } else {
            None
        }
    }

    /// Returns `true` if the object is [`String`].
    ///
    /// [`String`]: Object::String
    pub fn is_string(&self) -> bool {
        matches!(self, Self::String(..))
    }

    pub fn as_string(&self) -> Option<&StringObject> {
        if let Self::String(v) = self {
            Some(v)
        } else {
            None
        }
    }

    /// Returns `true` if the object is [`Builtin`].
    ///
    /// [`Builtin`]: Object::Builtin
    pub fn is_builtin(&self) -> bool {
        matches!(self, Self::Builtin(..))
    }

    pub fn as_builtin(&self) -> Option<&BuiltinObject> {
        if let Self::Builtin(v) = self {
            Some(v)
        } else {
            None
        }
    }

    /// Returns `true` if the object is [`Array`].
    ///
    /// [`Array`]: Object::Array
    pub fn is_array(&self) -> bool {
        matches!(self, Self::Array(..))
    }

    pub fn as_array(&self) -> Option<&ArrayObject> {
        if let Self::Array(v) = self {
            Some(v)
        } else {
            None
        }
    }
}

impl Display for Object {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Object::Boolean(o) => write!(f, "{}", o),
            Object::Integer(o) => write!(f, "{}", o),
            Object::Null(o) => write!(f, "{}", o),
            Object::ReturnValue(o) => write!(f, "{}", o),
            Object::Error(o) => write!(f, "{}", o),
            Object::Function(o) => write!(f, "{}", o),
            Object::String(o) => write!(f, "{}", o),
            Object::Builtin(o) => write!(f, "{}", o),
            Object::Array(o) => write!(f, "{}", o),
        }
    }
}

#[derive(Debug, Clone, PartialEq, Eq)]
pub struct IntegerObject {
    pub value: i64,
}

impl Display for IntegerObject {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", self.value)
    }
}

impl ObjectT for IntegerObject {
    fn ty(&self) -> ObjectType {
        INTEGER_OBJ
    }
}

#[derive(Debug, Clone, PartialEq, Eq)]
pub struct BooleanObject {
    pub value: bool,
}

impl Display for BooleanObject {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", if self.value { "true" } else { "false" })
    }
}

impl ObjectT for BooleanObject {
    fn ty(&self) -> ObjectType {
        BOOLEAN_OBJ
    }
}

#[derive(Debug, Clone, PartialEq, Eq)]
pub struct NullObject;

impl Display for NullObject {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "null")
    }
}

impl ObjectT for NullObject {
    fn ty(&self) -> ObjectType {
        NULL_OBJ
    }
}

#[derive(Debug, Clone, PartialEq, Eq)]
pub struct ReturnValueObject {
    pub value: Box<Object>,
}

impl Display for ReturnValueObject {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", self.value)
    }
}

impl ObjectT for ReturnValueObject {
    fn ty(&self) -> ObjectType {
        RETURN_VALUE_OBJ
    }
}

#[derive(Debug, Clone, PartialEq, Eq)]
pub struct ErrorObject {
    pub message: String,
}

impl Display for ErrorObject {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "ERROR: {}", self.message)
    }
}

impl ObjectT for ErrorObject {
    fn ty(&self) -> ObjectType {
        ERROR_OBJ
    }
}

#[derive(Debug, Clone, PartialEq, Eq)]
pub struct Environment {
    store: HashMap<String, Object>,
    outer: Option<Box<Environment>>,
}

impl Environment {
    pub fn new() -> Self {
        Self {
            store: HashMap::new(),
            outer: None,
        }
    }

    pub fn new_enclosed(outer: Box<Environment>) -> Self {
        Self {
            store: HashMap::new(),
            outer: Some(outer),
        }
    }

    pub fn get(&self, name: &str) -> Option<Object> {
        self.store
            .get(name)
            .cloned()
            .or_else(|| self.outer.as_ref().and_then(|o| o.get(name)))
    }

    pub fn set(&mut self, name: String, value: Object) -> Object {
        self.store.insert(name, value.clone());
        value
    }
}

impl Default for Environment {
    fn default() -> Self {
        Self::new()
    }
}

#[derive(Debug, Clone, PartialEq, Eq)]
pub struct FunctionObject {
    pub parameters: Vec<IdentifierExpression>,
    pub body: BlockStatement,
    pub env: Environment,
}

impl Display for FunctionObject {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "fn(")?;
        let mut first = true;
        for param in self.parameters.iter() {
            if first {
                first = false;
            } else {
                write!(f, ", ")?;
            }
            write!(f, "{}", param)?;
        }
        writeln!(f, ") {{")?;
        writeln!(f, "{}", self.body)?;
        write!(f, "}}")?;
        Ok(())
    }
}

impl ObjectT for FunctionObject {
    fn ty(&self) -> ObjectType {
        FUNCTION_OBJ
    }
}

#[derive(Debug, Clone, PartialEq, Eq)]
pub struct StringObject {
    pub value: String,
}

impl Display for StringObject {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", self.value)
    }
}

impl ObjectT for StringObject {
    fn ty(&self) -> ObjectType {
        STRING_OBJ
    }
}

#[derive(Debug, Clone, PartialEq, Eq)]
pub struct BuiltinObject {
    pub function: fn(Vec<Object>) -> Object,
}

impl Display for BuiltinObject {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "builtin function")
    }
}

impl ObjectT for BuiltinObject {
    fn ty(&self) -> ObjectType {
        BUILTIN_OBJ
    }
}

#[derive(Debug, Clone, PartialEq, Eq)]
pub struct ArrayObject {
    pub elements: Vec<Object>,
}

impl Display for ArrayObject {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "[")?;
        let mut first = true;
        for el in self.elements.iter() {
            if first {
                first = false;
            } else {
                write!(f, ", ")?;
            }
            write!(f, "{}", el)?;
        }
        write!(f, "]")?;
        Ok(())
    }
}

impl ObjectT for ArrayObject {
    fn ty(&self) -> ObjectType {
        ARRAY_OBJ
    }
}
