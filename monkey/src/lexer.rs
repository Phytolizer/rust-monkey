use std::iter::Peekable;
use std::str::Chars;

use crate::token;
use crate::token::Token;

#[cfg(test)]
mod tests;

pub struct Lexer<'input> {
    iter: Peekable<Chars<'input>>,
    peekq: [char; 2],
}

impl<'input> Lexer<'input> {
    pub fn new(input: &'input str) -> Self {
        Self {
            iter: input.chars().peekable(),
            peekq: ['\0'; 2],
        }
    }

    fn peek(&mut self, offset: usize) -> Option<char> {
        for i in 0..=offset {
            if self.peekq[i] == '\0' {
                match self.iter.next() {
                    Some(c) => {
                        self.peekq[i] = c;
                    }
                    None => return None,
                }
            }
        }
        Some(self.peekq[offset])
    }

    fn next(&mut self) -> Option<char> {
        if self.peekq[0] == '\0' {
            self.peekq[0] = match self.iter.next() {
                None => return None,
                Some(c) => c,
            }
        }
        let c = self.peekq[0];
        for i in 0..(self.peekq.len() - 1) {
            self.peekq[i] = self.peekq[i + 1];
        }
        self.peekq[self.peekq.len() - 1] = '\0';
        Some(c)
    }

    pub fn next_token(&mut self) -> Token {
        self.skip_whitespace();

        let mut literal = String::new();
        let ty = match self.peek(0) {
            Some(c) if is_letter(c) => {
                let ty = self.read_identifier(&mut literal);
                return Token { ty, literal };
            }
            Some(c) if c.is_digit(10) => {
                self.read_number(&mut literal);
                return Token {
                    ty: token::INT,
                    literal,
                };
            }
            Some('"') => {
                self.read_string(&mut literal);
                return Token {
                    ty: token::STRING,
                    literal,
                };
            }
            Some('=') if self.peek(1) == Some('=') => {
                literal.push(self.next().unwrap());
                token::EQ
            }
            Some('!') if self.peek(1) == Some('=') => {
                literal.push(self.next().unwrap());
                token::NOT_EQ
            }
            Some('=') => token::ASSIGN,
            Some(';') => token::SEMICOLON,
            Some('+') => token::PLUS,
            Some(',') => token::COMMA,
            Some('(') => token::LPAREN,
            Some(')') => token::RPAREN,
            Some('{') => token::LBRACE,
            Some('}') => token::RBRACE,
            Some('[') => token::LBRACKET,
            Some(']') => token::RBRACKET,
            Some('!') => token::BANG,
            Some('-') => token::MINUS,
            Some('/') => token::SLASH,
            Some('*') => token::ASTERISK,
            Some('<') => token::LT,
            Some('>') => token::GT,
            Some(_) => token::ILLEGAL,
            None => token::EOF,
        };
        if let Some(c) = self.next() {
            literal.push(c);
        }
        Token { ty, literal }
    }

    fn read_identifier(&mut self, literal: &mut String) -> token::TokenType {
        while let Some(c) = self.peek(0) {
            if !c.is_alphabetic() {
                break;
            }
            literal.push(self.next().unwrap());
        }
        token::lookup_ident(literal)
    }

    fn skip_whitespace(&mut self) {
        while let Some(c) = self.peek(0) {
            if !c.is_whitespace() {
                break;
            }
            self.next();
        }
    }

    fn read_number(&mut self, literal: &mut String) {
        while let Some(c) = self.peek(0) {
            if !c.is_digit(10) {
                break;
            }
            literal.push(self.next().unwrap());
        }
    }

    fn read_string(&mut self, literal: &mut String) {
        self.next();
        loop {
            if let Some('"') | None = self.peek(0) {
                break;
            }
            literal.push(self.next().unwrap());
        }
        self.next();
    }
}

fn is_letter(c: char) -> bool {
    c.is_alphabetic() || c == '_'
}
