use crate::ast::Expression;
use crate::ast::ExpressionStatement;
use crate::ast::IdentifierExpression;
use crate::ast::NodeT;
use crate::ast::Program;
use crate::ast::Statement;
use crate::lexer::Lexer;
use crate::test_object::TestObject;
use crate::token::Token;

use super::Parser;

macro_rules! o {
    ($val:expr) => {
        TestObject::from($val)
    };
}

#[test]
fn test_let_statements() {
    struct Test {
        input: &'static str,
        expected_identifier: &'static str,
        expected_value: TestObject,
    }

    impl Test {
        const fn new(
            input: &'static str,
            expected_identifier: &'static str,
            expected_value: TestObject,
        ) -> Self {
            Self {
                input,
                expected_identifier,
                expected_value,
            }
        }
    }
    let tests = &[
        Test::new("let x = 5;", "x", o!(5)),
        Test::new("let y = true;", "y", o!(true)),
        Test::new("let foobar = y;", "foobar", o!("y")),
    ];

    for tt in tests {
        let program = parse(tt.input);
        assert_eq!(program.statements.len(), 1);
        let stmt = &program.statements[0];
        test_let_statement(stmt, tt.expected_identifier, &tt.expected_value);
    }
}

#[test]
fn test_return_statements() {
    let tests = [
        ("return 5;", o!(5)),
        ("return true;", o!(true)),
        ("return foobar;", o!("foobar")),
    ];

    for (input, expected_return_value) in tests {
        let program = parse(input);
        assert_eq!(program.statements.len(), 1);
        let stmt = match &program.statements[0] {
            Statement::Return(s) => s,
            s => panic!("not a return statement: {}", s),
        };

        assert_eq!(stmt.token_literal(), "return");
        test_literal_expression(&stmt.return_value, &expected_return_value);
    }
}

#[test]
fn test_identifier_expression() {
    const INPUT: &str = "foobar;";
    let exp = parse_expression(INPUT);
    test_identifier(&exp, "foobar");
}

#[test]
fn test_integer_literal_expression() {
    const INPUT: &str = "5;";
    let exp = parse_expression(INPUT);
    let literal = match exp {
        Expression::IntegerLiteral(lit) => lit,
        e => panic!("not an integer literal expression: {}", e),
    };
    assert_eq!(literal.value, 5);
    assert_eq!(literal.token_literal(), "5");
}

#[test]
fn test_boolean_expression() {
    const INPUT: &str = "true;";
    let exp = parse_expression(INPUT);
    let literal = match exp {
        Expression::Boolean(b) => b,
        _ => panic!("not a boolean expression: {}", exp),
    };
    assert!(literal.value);
    assert_eq!(literal.token_literal(), "true");
}

#[test]
fn test_parsing_prefix_expressions() {
    struct Test {
        input: &'static str,
        operator: &'static str,
        value: TestObject,
    }

    impl Test {
        const fn new(input: &'static str, operator: &'static str, value: TestObject) -> Self {
            Self {
                input,
                operator,
                value,
            }
        }
    }

    let tests = &[
        Test::new("!5;", "!", o!(5)),
        Test::new("-15;", "-", o!(15)),
        Test::new("!true;", "!", o!(true)),
        Test::new("!false;", "!", o!(false)),
    ];

    for tt in tests {
        let exp = parse_expression(tt.input);
        let exp = match exp {
            Expression::Prefix(prefix) => prefix,
            _ => panic!("not a prefix expression: {}", exp),
        };
        assert_eq!(exp.operator, tt.operator);
        test_literal_expression(&exp.right, &tt.value);
    }
}

#[test]
fn test_parsing_infix_expressions() {
    struct Test {
        input: &'static str,
        left_value: TestObject,
        operator: &'static str,
        right_value: TestObject,
    }

    impl Test {
        const fn new(
            input: &'static str,
            left_value: TestObject,
            operator: &'static str,
            right_value: TestObject,
        ) -> Self {
            Self {
                input,
                left_value,
                operator,
                right_value,
            }
        }
    }

    let tests: &[Test] = &[
        Test::new("5 + 5;", o!(5), "+", o!(5)),
        Test::new("5 - 5;", o!(5), "-", o!(5)),
        Test::new("5 * 5;", o!(5), "*", o!(5)),
        Test::new("5 / 5;", o!(5), "/", o!(5)),
        Test::new("5 > 5;", o!(5), ">", o!(5)),
        Test::new("5 < 5;", o!(5), "<", o!(5)),
        Test::new("5 == 5;", o!(5), "==", o!(5)),
        Test::new("5 != 5;", o!(5), "!=", o!(5)),
        Test::new("true == true", o!(true), "==", o!(true)),
        Test::new("true != false", o!(true), "!=", o!(false)),
        Test::new("false == false", o!(false), "==", o!(false)),
    ];

    for tt in tests {
        let exp = parse_expression(tt.input);
        test_infix_expression(&exp, &tt.left_value, tt.operator, &tt.right_value);
    }
}

#[test]
fn test_operator_precedence_parsing() {
    struct Test {
        input: &'static str,
        expected: &'static str,
    }

    impl Test {
        const fn new(input: &'static str, expected: &'static str) -> Self {
            Self { input, expected }
        }
    }

    const TESTS: &[Test] = &[
        Test::new("-a * b", "((-a) * b);"),
        Test::new("!-a", "(!(-a));"),
        Test::new("a + b + c", "((a + b) + c);"),
        Test::new("a + b - c", "((a + b) - c);"),
        Test::new("a * b * c", "((a * b) * c);"),
        Test::new("a * b / c", "((a * b) / c);"),
        Test::new("a + b / c", "(a + (b / c));"),
        Test::new("a + b * c + d / e - f", "(((a + (b * c)) + (d / e)) - f);"),
        Test::new("3 + 4; -5 * 5", "(3 + 4);((-5) * 5);"),
        Test::new("5 > 4 == 3 < 4", "((5 > 4) == (3 < 4));"),
        Test::new("5 < 4 != 3 > 4", "((5 < 4) != (3 > 4));"),
        Test::new(
            "3 + 4 * 5 == 3 * 1 + 4 * 5",
            "((3 + (4 * 5)) == ((3 * 1) + (4 * 5)));",
        ),
        Test::new(
            "3 + 4 * 5 == 3 * 1 + 4 * 5",
            "((3 + (4 * 5)) == ((3 * 1) + (4 * 5)));",
        ),
        Test::new("true", "true;"),
        Test::new("false", "false;"),
        Test::new("3 > 5 == false", "((3 > 5) == false);"),
        Test::new("3 < 5 == true", "((3 < 5) == true);"),
        Test::new("1 + (2 + 3) + 4", "((1 + (2 + 3)) + 4);"),
        Test::new("(5 + 5) * 2", "((5 + 5) * 2);"),
        Test::new("2 / (5 + 5)", "(2 / (5 + 5));"),
        Test::new("-(5 + 5)", "(-(5 + 5));"),
        Test::new("!(true == true)", "(!(true == true));"),
        Test::new("a + add(b * c) + d", "((a + add((b * c))) + d);"),
        Test::new(
            "add(a, b, 1, 2 * 3, 4 + 5, add(6, 7 * 8))",
            "add(a, b, 1, (2 * 3), (4 + 5), add(6, (7 * 8)));",
        ),
        Test::new(
            "add(a + b + c * d / f + g)",
            "add((((a + b) + ((c * d) / f)) + g));",
        ),
        Test::new(
            "a * [1, 2, 3, 4][b * c] * d",
            "((a * ([1, 2, 3, 4][(b * c)])) * d);",
        ),
        Test::new(
            "add(a * b[2], b[1], 2 * [1, 2][1])",
            "add((a * (b[2])), (b[1]), (2 * ([1, 2][1])));",
        ),
    ];

    for tt in TESTS {
        let program = parse(tt.input);
        let actual = program.to_string();
        assert_eq!(actual, tt.expected);
    }
}

#[test]
fn test_if_expression() {
    const INPUT: &str = "if (x < y) { x }";
    let exp = parse_expression(INPUT);
    let exp = match exp {
        Expression::If(i) => i,
        _ => panic!("not an if expression: {}", exp),
    };
    assert_eq!(exp.token_literal(), "if");
    test_infix_expression(&exp.condition, &o!("x"), "<", &o!("y"));
    assert_eq!(exp.consequence.statements.len(), 1);
    let consequence = match &exp.consequence.statements[0] {
        Statement::Expression(e) => e,
        s => panic!("not an expression statement: {}", s),
    };
    test_identifier(&consequence.expression, "x");
    assert!(exp.alternative.is_none());
}

#[test]
fn test_if_else_expression() {
    const INPUT: &str = "if (x < y) { x } else { y }";
    let exp = parse_expression(INPUT);
    let exp = match exp {
        Expression::If(i) => i,
        _ => panic!("not an if expression: {}", exp),
    };
    assert_eq!(exp.token_literal(), "if");
    test_infix_expression(&exp.condition, &o!("x"), "<", &o!("y"));
    assert_eq!(exp.consequence.statements.len(), 1);
    let consequence = match &exp.consequence.statements[0] {
        Statement::Expression(e) => e,
        s => panic!("not an expression statement: {}", s),
    };
    test_identifier(&consequence.expression, "x");
    let alternative = exp.alternative.unwrap();
    assert_eq!(alternative.statements.len(), 1);
    let alternative = match &alternative.statements[0] {
        Statement::Expression(e) => e,
        s => panic!("not an expression statement: {}", s),
    };
    test_identifier(&alternative.expression, "y");
}

#[test]
fn test_function_literal_parsing() {
    const INPUT: &str = "fn(x, y) { x + y; }";
    let exp = parse_expression(INPUT);
    let exp = match exp {
        Expression::Function(f) => f,
        _ => panic!("not a function expression: {}", exp),
    };
    assert_eq!(exp.token_literal(), "fn");
    assert_eq!(exp.parameters.len(), 2);
    assert_eq!(&exp.parameters[0].value, "x");
    assert_eq!(&exp.parameters[1].value, "y");

    assert_eq!(exp.body.statements.len(), 1);
    let body = match &exp.body.statements[0] {
        Statement::Expression(e) => e,
        s => panic!("not an expression statement: {}", s),
    };
    test_infix_expression(&body.expression, &o!("x"), "+", &o!("y"));
}

#[test]
fn test_function_parameters_parsing() {
    for (input, expected_params) in [
        ("fn() {};", vec![]),
        ("fn(x) {};", vec!["x"]),
        ("fn(x, y, z) {};", vec!["x", "y", "z"]),
    ] {
        let func = match parse_expression(input) {
            Expression::Function(f) => f,
            e => panic!("not a function expression: {}", e),
        };
        assert_eq!(func.parameters.len(), expected_params.len());
        for (act, exp) in func.parameters.iter().zip(expected_params.into_iter()) {
            assert_eq!(act.to_string(), exp);
        }
    }
}

#[test]
fn test_call_expression_parsing() {
    const INPUT: &str = "add(1, 2 * 3, 4 + 5)";
    let exp = parse_expression(INPUT);
    let exp = match exp {
        Expression::Call(c) => c,
        _ => panic!("not a call expression: {}", exp),
    };
    test_identifier(&exp.function, "add");
    assert_eq!(exp.arguments.len(), 3);
    test_literal_expression(&exp.arguments[0], &o!(1));
    test_infix_expression(&exp.arguments[1], &o!(2), "*", &o!(3));
    test_infix_expression(&exp.arguments[2], &o!(4), "+", &o!(5));
}

#[test]
fn test_string_literal_expression() {
    const INPUT: &str = r#""hello world";"#;
    let exp = parse_expression(INPUT);
    let exp = match exp {
        Expression::String(s) => s,
        _ => panic!("not a string expression: {}", exp),
    };
    assert_eq!(exp.value, "hello world");
}

#[test]
fn test_parsing_array_literals() {
    const INPUT: &str = "[1, 2 * 2, 3 + 3];";
    let exp = parse_expression(INPUT);
    let array = match exp {
        Expression::Array(a) => a,
        _ => panic!("not an array: {}", exp),
    };
    assert_eq!(array.elements.len(), 3);
    test_literal_expression(&array.elements[0], &o!(1));
    test_infix_expression(&array.elements[1], &o!(2), "*", &o!(2));
    test_infix_expression(&array.elements[2], &o!(3), "+", &o!(3));
}

#[test]
fn test_parsing_index_expressions() {
    const INPUT: &str = "myArray[1 + 1]";
    let exp = match parse_expression(INPUT) {
        Expression::Index(i) => i,
        e => panic!("not an index expression: {}", e),
    };
    test_identifier(&exp.left, "myArray");
    test_infix_expression(&exp.index, &o!(1), "+", &o!(1));
}

fn test_infix_expression(exp: &Expression, left: &TestObject, operator: &str, right: &TestObject) {
    let infix = match exp {
        Expression::Infix(i) => i,
        _ => panic!("not an infix expression: {}", exp),
    };
    test_literal_expression(&infix.left, left);
    assert_eq!(infix.operator, operator);
    test_literal_expression(&infix.right, right);
}

fn test_identifier(exp: &Expression, expected: &str) {
    let ident = match exp {
        Expression::Identifier(i) => i,
        _ => panic!("not an identifier: {}", exp),
    };
    assert_eq!(ident.value, expected);
    assert_eq!(ident.token_literal(), expected);
}

fn test_boolean_literal(exp: &Expression, expected: bool) {
    let b = match exp {
        Expression::Boolean(b) => b,
        _ => panic!("not a boolean literal: {}", exp),
    };
    assert_eq!(b.value, expected);
    assert_eq!(b.token_literal(), expected.to_string());
}

fn test_literal_expression(exp: &Expression, expected: &TestObject) {
    match expected {
        TestObject::Bool(b) => test_boolean_literal(exp, *b),
        TestObject::Int(i) => test_integer_literal(exp, *i),
        TestObject::Ident(i) => test_identifier(exp, i),
        _ => unreachable!(),
    }
}

fn test_integer_literal(right: &Expression, value: i64) {
    let integ = match right {
        Expression::IntegerLiteral(i) => i,
        _ => panic!("not an integer literal: {}", right),
    };
    assert_eq!(integ.value, value);
    assert_eq!(integ.token_literal(), value.to_string());
}

fn parse_expression(input: &str) -> Expression {
    let mut program = parse(input);
    assert_eq!(program.statements.len(), 1);
    let mut stmt = Statement::Expression(ExpressionStatement {
        token: Token::default(),
        expression: Expression::Identifier(IdentifierExpression {
            token: Token::default(),
            value: String::new(),
        }),
    });
    std::mem::swap(&mut stmt, &mut program.statements[0]);
    let expr = match stmt {
        Statement::Expression(s) => s,
        s => panic!("not an expression: {}", s),
    };
    expr.expression
}

fn parse(text: &str) -> Program {
    let l = Lexer::new(text);
    let mut p = Parser::new(l);
    let program = p.parse_program();
    check_parser_errors(&p);
    program
}

fn check_parser_errors(p: &Parser) {
    for error in p.errors() {
        eprintln!("parser error: {}", error);
    }
    assert!(p.errors().is_empty());
}

fn test_let_statement(stmt: &Statement, expected_identifier: &str, expected_value: &TestObject) {
    assert_eq!(stmt.token_literal(), "let");
    let stmt = match stmt {
        Statement::Let(s) => s,
        _ => panic!("not a let statement: {}", stmt),
    };
    assert_eq!(stmt.name.value, expected_identifier);
    assert_eq!(stmt.name.token_literal(), expected_identifier);
    test_literal_expression(&stmt.value, expected_value);
}
