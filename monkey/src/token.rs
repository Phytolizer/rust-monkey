use std::fmt::Display;

#[derive(Debug, Clone, Copy, PartialEq, Eq, Hash)]
pub struct TokenType(&'static str);

impl Display for TokenType {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", self.0)
    }
}

pub const ILLEGAL: TokenType = TokenType("ILLEGAL");
pub const EOF: TokenType = TokenType("EOF");

pub const IDENT: TokenType = TokenType("IDENT");
pub const INT: TokenType = TokenType("INT");
pub const STRING: TokenType = TokenType("STRING");

pub const ASSIGN: TokenType = TokenType("=");
pub const PLUS: TokenType = TokenType("+");
pub const BANG: TokenType = TokenType("!");
pub const MINUS: TokenType = TokenType("-");
pub const ASTERISK: TokenType = TokenType("*");
pub const SLASH: TokenType = TokenType("/");

pub const LT: TokenType = TokenType("<");
pub const GT: TokenType = TokenType(">");
pub const EQ: TokenType = TokenType("==");
pub const NOT_EQ: TokenType = TokenType("!=");

pub const COMMA: TokenType = TokenType(",");
pub const SEMICOLON: TokenType = TokenType(";");

pub const LPAREN: TokenType = TokenType("(");
pub const RPAREN: TokenType = TokenType(")");
pub const LBRACE: TokenType = TokenType("{");
pub const RBRACE: TokenType = TokenType("}");
pub const LBRACKET: TokenType = TokenType("[");
pub const RBRACKET: TokenType = TokenType("]");

pub const ELSE: TokenType = TokenType("ELSE");
pub const FALSE: TokenType = TokenType("FALSE");
pub const FUNCTION: TokenType = TokenType("FUNCTION");
pub const IF: TokenType = TokenType("IF");
pub const LET: TokenType = TokenType("LET");
pub const RETURN: TokenType = TokenType("RETURN");
pub const TRUE: TokenType = TokenType("TRUE");

#[derive(Debug, Clone, PartialEq, Eq)]
pub struct Token {
    pub ty: TokenType,
    pub literal: String,
}

impl Display for Token {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{{Type:{} Literal:{}}}", self.ty, self.literal)
    }
}

impl Default for Token {
    fn default() -> Self {
        Self {
            ty: ILLEGAL,
            literal: String::new(),
        }
    }
}

pub fn lookup_ident(ident: &str) -> TokenType {
    match ident {
        "else" => ELSE,
        "false" => FALSE,
        "fn" => FUNCTION,
        "if" => IF,
        "let" => LET,
        "return" => RETURN,
        "true" => TRUE,
        _ => IDENT,
    }
}
