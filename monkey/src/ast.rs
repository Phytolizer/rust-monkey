use std::fmt::Display;

use enum_dispatch::enum_dispatch;

use crate::token::Token;

#[enum_dispatch]
pub trait NodeT: Display {
    fn token_literal(&self) -> &str;
}

#[enum_dispatch(NodeT)]
pub enum Node {
    Expression(Expression),
    Statement(Statement),
    Program(Program),
}

impl Display for Node {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Node::Expression(n) => write!(f, "{}", n),
            Node::Statement(n) => write!(f, "{}", n),
            Node::Program(n) => write!(f, "{}", n),
        }
    }
}

pub struct Program {
    pub statements: Vec<Statement>,
}

impl Display for Program {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        for stmt in self.statements.iter() {
            write!(f, "{}", stmt)?;
        }
        Ok(())
    }
}

impl NodeT for Program {
    fn token_literal(&self) -> &str {
        if let Some(s) = self.statements.first() {
            s.token_literal()
        } else {
            ""
        }
    }
}

#[enum_dispatch]
pub trait ExpressionT: NodeT {}

#[enum_dispatch(ExpressionT)]
#[derive(Debug, Clone, PartialEq, Eq)]
pub enum Expression {
    Boolean(BooleanExpression),
    Call(CallExpression),
    Function(FunctionExpression),
    Identifier(IdentifierExpression),
    If(IfExpression),
    Infix(InfixExpression),
    IntegerLiteral(IntegerLiteralExpression),
    Prefix(PrefixExpression),
    String(StringExpression),
    Array(ArrayExpression),
    Index(IndexExpression),
}

impl Display for Expression {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Expression::Boolean(e) => write!(f, "{}", e),
            Expression::Call(e) => write!(f, "{}", e),
            Expression::Function(e) => write!(f, "{}", e),
            Expression::Identifier(e) => write!(f, "{}", e),
            Expression::If(e) => write!(f, "{}", e),
            Expression::Infix(e) => write!(f, "{}", e),
            Expression::IntegerLiteral(e) => write!(f, "{}", e),
            Expression::Prefix(e) => write!(f, "{}", e),
            Expression::String(e) => write!(f, "{}", e),
            Expression::Array(e) => write!(f, "{}", e),
            Expression::Index(e) => write!(f, "{}", e),
        }
    }
}

impl NodeT for Expression {
    fn token_literal(&self) -> &str {
        match self {
            Expression::Boolean(e) => e.token_literal(),
            Expression::Call(e) => e.token_literal(),
            Expression::Function(e) => e.token_literal(),
            Expression::Identifier(e) => e.token_literal(),
            Expression::If(e) => e.token_literal(),
            Expression::Infix(e) => e.token_literal(),
            Expression::IntegerLiteral(e) => e.token_literal(),
            Expression::Prefix(e) => e.token_literal(),
            Expression::String(e) => e.token_literal(),
            Expression::Array(e) => e.token_literal(),
            Expression::Index(e) => e.token_literal(),
        }
    }
}

#[enum_dispatch]
pub trait StatementT: NodeT {}

#[enum_dispatch(StatementT)]
#[derive(Debug, Clone, PartialEq, Eq)]
pub enum Statement {
    Block(BlockStatement),
    Expression(ExpressionStatement),
    Let(LetStatement),
    Return(ReturnStatement),
}

impl Display for Statement {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Statement::Block(s) => write!(f, "{}", s),
            Statement::Expression(s) => write!(f, "{}", s),
            Statement::Let(s) => write!(f, "{}", s),
            Statement::Return(s) => write!(f, "{}", s),
        }
    }
}

impl NodeT for Statement {
    fn token_literal(&self) -> &str {
        match self {
            Statement::Block(s) => s.token_literal(),
            Statement::Expression(s) => s.token_literal(),
            Statement::Let(s) => s.token_literal(),
            Statement::Return(s) => s.token_literal(),
        }
    }
}

#[derive(Debug, Clone, PartialEq, Eq)]
pub struct LetStatement {
    pub token: Token,
    pub name: IdentifierExpression,
    pub value: Expression,
}

impl Display for LetStatement {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(
            f,
            "{} {} = {};",
            self.token_literal(),
            self.name,
            self.value
        )
    }
}

impl NodeT for LetStatement {
    fn token_literal(&self) -> &str {
        &self.token.literal
    }
}

#[derive(Debug, Clone, PartialEq, Eq)]
pub struct IdentifierExpression {
    pub token: Token,
    pub value: String,
}

impl Display for IdentifierExpression {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", self.value)
    }
}

impl NodeT for IdentifierExpression {
    fn token_literal(&self) -> &str {
        &self.token.literal
    }
}

#[derive(Debug, Clone, PartialEq, Eq)]
pub struct IntegerLiteralExpression {
    pub token: Token,
    pub value: i64,
}

impl Display for IntegerLiteralExpression {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", self.value)
    }
}

impl NodeT for IntegerLiteralExpression {
    fn token_literal(&self) -> &str {
        &self.token.literal
    }
}

#[derive(Debug, Clone, PartialEq, Eq)]
pub struct PrefixExpression {
    pub token: Token,
    pub operator: String,
    pub right: Box<Expression>,
}

impl Display for PrefixExpression {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "({}{})", self.operator, self.right)
    }
}

impl NodeT for PrefixExpression {
    fn token_literal(&self) -> &str {
        &self.token.literal
    }
}

#[derive(Debug, Clone, PartialEq, Eq)]
pub struct InfixExpression {
    pub token: Token,
    pub left: Box<Expression>,
    pub operator: String,
    pub right: Box<Expression>,
}

impl Display for InfixExpression {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "({} {} {})", self.left, self.operator, self.right)
    }
}

impl NodeT for InfixExpression {
    fn token_literal(&self) -> &str {
        &self.token.literal
    }
}

#[derive(Debug, Clone, PartialEq, Eq)]
pub struct BooleanExpression {
    pub token: Token,
    pub value: bool,
}

impl Display for BooleanExpression {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", self.value)
    }
}

impl NodeT for BooleanExpression {
    fn token_literal(&self) -> &str {
        &self.token.literal
    }
}

#[derive(Debug, Clone, PartialEq, Eq)]
pub struct IfExpression {
    pub token: Token,
    pub condition: Box<Expression>,
    pub consequence: Box<BlockStatement>,
    pub alternative: Option<Box<BlockStatement>>,
}

impl Display for IfExpression {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "if")?;
        write!(f, "{}", self.condition)?;
        write!(f, " ")?;
        write!(f, "{}", self.consequence)?;
        if let Some(alternative) = self.alternative.as_ref() {
            write!(f, " else ")?;
            write!(f, "{}", alternative)?;
        }
        Ok(())
    }
}

impl NodeT for IfExpression {
    fn token_literal(&self) -> &str {
        &self.token.literal
    }
}

#[derive(Debug, Clone, PartialEq, Eq)]
pub struct FunctionExpression {
    pub token: Token,
    pub parameters: Vec<IdentifierExpression>,
    pub body: Box<BlockStatement>,
}

impl Display for FunctionExpression {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", self.token_literal())?;
        write!(f, "(")?;
        let mut first = true;
        for param in self.parameters.iter() {
            if first {
                first = false;
            } else {
                write!(f, ", ")?;
            }
            write!(f, "{}", param)?;
        }
        write!(f, ")")?;
        Ok(())
    }
}

impl NodeT for FunctionExpression {
    fn token_literal(&self) -> &str {
        &self.token.literal
    }
}

#[derive(Debug, Clone, PartialEq, Eq)]
pub struct CallExpression {
    pub token: Token,
    pub function: Box<Expression>,
    pub arguments: Vec<Expression>,
}

impl Display for CallExpression {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}(", self.function)?;
        let mut first = true;
        for arg in &self.arguments {
            if first {
                first = false;
            } else {
                write!(f, ", ")?;
            }
            write!(f, "{}", arg)?;
        }
        write!(f, ")")?;
        Ok(())
    }
}

impl NodeT for CallExpression {
    fn token_literal(&self) -> &str {
        &self.token.literal
    }
}

#[derive(Debug, Clone, PartialEq, Eq)]
pub struct ReturnStatement {
    pub token: Token,
    pub return_value: Expression,
}

impl Display for ReturnStatement {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "return {};", self.return_value)
    }
}

impl NodeT for ReturnStatement {
    fn token_literal(&self) -> &str {
        &self.token.literal
    }
}

#[derive(Debug, Clone, PartialEq, Eq)]
pub struct ExpressionStatement {
    pub token: Token,
    pub expression: Expression,
}

impl Display for ExpressionStatement {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", self.expression)?;
        write!(f, ";")?;
        Ok(())
    }
}

impl NodeT for ExpressionStatement {
    fn token_literal(&self) -> &str {
        &self.token.literal
    }
}

#[derive(Debug, Clone, PartialEq, Eq)]
pub struct BlockStatement {
    pub token: Token,
    pub statements: Vec<Statement>,
}

impl Display for BlockStatement {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        for stmt in &self.statements {
            write!(f, "{}", stmt)?;
        }
        Ok(())
    }
}

impl NodeT for BlockStatement {
    fn token_literal(&self) -> &str {
        &self.token.literal
    }
}

#[derive(Debug, Clone, PartialEq, Eq)]
pub struct StringExpression {
    pub token: Token,
    pub value: String,
}

impl Display for StringExpression {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "{}", self.value)
    }
}

impl NodeT for StringExpression {
    fn token_literal(&self) -> &str {
        &self.token.literal
    }
}

#[derive(Debug, Clone, PartialEq, Eq)]
pub struct ArrayExpression {
    pub token: Token,
    pub elements: Vec<Expression>,
}

impl Display for ArrayExpression {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "[")?;
        let mut first = true;
        for el in self.elements.iter() {
            if first {
                first = false;
            } else {
                write!(f, ", ")?;
            }
            write!(f, "{}", el)?;
        }
        write!(f, "]")?;
        Ok(())
    }
}

impl NodeT for ArrayExpression {
    fn token_literal(&self) -> &str {
        &self.token.literal
    }
}

#[derive(Debug, Clone, PartialEq, Eq)]
pub struct IndexExpression {
    pub token: Token,
    pub left: Box<Expression>,
    pub index: Box<Expression>,
}

impl Display for IndexExpression {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        write!(f, "(")?;
        write!(f, "{}", self.left)?;
        write!(f, "[")?;
        write!(f, "{}", self.index)?;
        write!(f, "])")?;
        Ok(())
    }
}

impl NodeT for IndexExpression {
    fn token_literal(&self) -> &str {
        &self.token.literal
    }
}

#[cfg(test)]
mod tests;
