use crate::ast::Expression;
use crate::ast::IdentifierExpression;
use crate::ast::LetStatement;
use crate::ast::Statement;
use crate::token;
use crate::token::Token;

use super::Program;

#[test]
fn test_string() {
    let program = Program {
        statements: vec![Statement::Let(LetStatement {
            token: Token {
                ty: token::LET,
                literal: String::from("let"),
            },
            name: IdentifierExpression {
                token: Token {
                    ty: token::IDENT,
                    literal: String::from("myVar"),
                },
                value: String::from("myVar"),
            },
            value: Expression::Identifier(IdentifierExpression {
                token: Token {
                    ty: token::IDENT,
                    literal: String::from("anotherVar"),
                },
                value: String::from("anotherVar"),
            }),
        })],
    };

    assert_eq!(program.to_string(), String::from("let myVar = anotherVar;"));
}
