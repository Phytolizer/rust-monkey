pub(crate) enum TestObject {
    Bool(bool),
    Int(i64),
    Ident(&'static str),
    Null,
}

impl From<bool> for TestObject {
    fn from(b: bool) -> Self {
        Self::Bool(b)
    }
}

impl From<i64> for TestObject {
    fn from(i: i64) -> Self {
        Self::Int(i)
    }
}

impl From<&'static str> for TestObject {
    fn from(s: &'static str) -> Self {
        Self::Ident(s)
    }
}
