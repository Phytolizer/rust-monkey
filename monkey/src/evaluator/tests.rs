use crate::ast::Node;
use crate::evaluator::eval;
use crate::lexer::Lexer;
use crate::object::Environment;
use crate::object::Object;
use crate::parser::Parser;
use crate::test_object::TestObject;

macro_rules! o {
    (null) => {
        TestObject::Null
    };
    ($val:expr) => {
        TestObject::from($val)
    };
}

fn test_eval(input: &str) -> Object {
    let l = Lexer::new(input);
    let mut p = Parser::new(l);
    let program = p.parse_program();
    for error in p.errors() {
        eprintln!("parse error: {}", error);
    }
    assert!(p.errors().is_empty());
    eval(Node::Program(program), &mut Environment::new())
}

#[test]
fn test_eval_integer_expression() {
    for (input, expected) in [
        ("5", 5),
        ("10", 10),
        ("-5", -5),
        ("-10", -10),
        ("5 + 5 + 5 + 5 - 10", 10),
        ("2 * 2 * 2 * 2 * 2", 32),
        ("-50 + 100 + -50", 0),
        ("5 * 2 + 10", 20),
        ("5 + 2 * 10", 25),
        ("20 + 2 * -10", 0),
        ("50 / 2 * 2 + 10", 60),
        ("2 * (5 + 10)", 30),
        ("3 * 3 * 3 + 10", 37),
        ("3 * (3 * 3) + 10", 37),
        ("(5 + 10 * 2 + 15 / 3) * 2 + -10", 50),
    ] {
        test_object(test_eval(input), TestObject::Int(expected));
    }
}

#[test]
fn test_eval_boolean_expression() {
    for (input, expected) in [
        ("true", true),
        ("false", false),
        ("1 < 2", true),
        ("1 > 2", false),
        ("1 < 1", false),
        ("1 > 1", false),
        ("1 == 1", true),
        ("1 != 1", false),
        ("1 == 2", false),
        ("1 != 2", true),
        ("true == true", true),
        ("false == false", true),
        ("true == false", false),
        ("true != false", true),
        ("false != true", true),
        ("(1 < 2) == true", true),
        ("(1 < 2) == false", false),
        ("(1 > 2) == true", false),
        ("(1 > 2) == false", true),
    ] {
        test_object(test_eval(input), TestObject::Bool(expected));
    }
}

#[test]
fn test_bang_operator() {
    for (input, expected) in [
        ("!true", false),
        ("!false", true),
        ("!5", false),
        ("!!true", true),
        ("!!false", false),
        ("!!5", true),
    ] {
        test_object(test_eval(input), TestObject::Bool(expected));
    }
}

#[test]
fn test_if_else_expressions() {
    for (input, expected) in [
        ("if (true) { 10 }", o!(10)),
        ("if (false) { 10 }", o!(null)),
        ("if (1) { 10 }", o!(10)),
        ("if (1 < 2) { 10 }", o!(10)),
        ("if (1 > 2) { 10 }", o!(null)),
        ("if (1 > 2) { 10 } else { 20 }", o!(20)),
        ("if (1 < 2) { 10 } else { 20 }", o!(10)),
    ] {
        test_object(test_eval(input), expected);
    }
}

#[test]
fn test_return_statements() {
    for (input, expected) in [
        ("return 10;", 10),
        ("return 10; 9;", 10),
        ("return 2 * 5; 9;", 10),
        ("9; return 2 * 5; 9;", 10),
        (
            "
            if (10 > 1) {
                if (10 > 1) {
                    return 10;
                }

                return 1;
            }
            ",
            10,
        ),
    ] {
        test_object(test_eval(input), TestObject::Int(expected));
    }
}

#[test]
fn test_error_handling() {
    for (input, expected_message) in [
        ("5 + true;", "type mismatch: INTEGER + BOOLEAN"),
        ("5 + true; 5;", "type mismatch: INTEGER + BOOLEAN"),
        ("-true", "unknown operator: -BOOLEAN"),
        ("true + false;", "unknown operator: BOOLEAN + BOOLEAN"),
        ("5; true + false; 5", "unknown operator: BOOLEAN + BOOLEAN"),
        (
            "if (10 > 1) { true + false; }",
            "unknown operator: BOOLEAN + BOOLEAN",
        ),
        (
            "
            if (10 > 1) {
                if (10 > 1) {
                    return true + false;
                }

                return 1;
            }
            ",
            "unknown operator: BOOLEAN + BOOLEAN",
        ),
        ("foobar", "identifier not found: foobar"),
        (r#""Hello" - "World""#, "unknown operator: STRING - STRING"),
    ] {
        test_error(test_eval(input), expected_message);
    }
}

#[test]
fn test_let_statements() {
    for (input, expected) in [
        ("let a = 5; a;", 5),
        ("let a = 5 * 5; a;", 25),
        ("let a = 5; let b = a; b;", 5),
        ("let a = 5; let b = a; let c = a + b + 5; c;", 15),
    ] {
        test_object(test_eval(input), TestObject::Int(expected));
    }
}

#[test]
fn test_function_objects() {
    const INPUT: &str = "fn(x) { x + 2; };";
    let evaluated = test_eval(INPUT);
    let func = match evaluated {
        Object::Function(f) => f,
        _ => panic!("not a function: {}", evaluated),
    };
    assert_eq!(func.parameters.len(), 1);
    assert_eq!(func.parameters[0].to_string(), "x");
    assert_eq!(func.body.to_string(), "(x + 2);");
}

#[test]
fn test_function_application() {
    for (input, expected) in [
        ("let identity = fn(x) { x; }; identity(5);", 5),
        ("let identity = fn(x) { return x; }; identity(5);", 5),
        ("let double = fn(x) { x * 2; }; double(5);", 10),
        ("let add = fn(x, y) { x + y; }; add(5, 5);", 10),
        ("let add = fn(x, y) { x + y; }; add(5 + 5, add(5, 5));", 20),
        ("fn(x) { x; }(5)", 5),
    ] {
        test_object(test_eval(input), TestObject::Int(expected));
    }
}

#[test]
fn test_closures() {
    const INPUT: &str = "
        let newAdder = fn(x) {
            fn(y) { x + y };
        };
        let addTwo = newAdder(2);
        addTwo(2);
    ";
    test_object(test_eval(INPUT), TestObject::Int(4));
}

#[test]
fn test_string_literal() {
    const INPUT: &str = r#""Hello World!""#;
    let evaluated = match test_eval(INPUT) {
        Object::String(s) => s,
        e => panic!("not a string: {}", e),
    };
    assert_eq!(evaluated.value, "Hello World!");
}

#[test]
fn test_string_concatenation() {
    const INPUT: &str = r#""Hello" + " " + "World!""#;
    let evaluated = match test_eval(INPUT) {
        Object::String(s) => s,
        e => panic!("not a string: {}", e),
    };
    assert_eq!(evaluated.value, "Hello World!");
}

#[test]
fn test_builtin_functions() {
    for (text, expected) in [
        (r#"len("")"#, o!(0)),
        (r#"len("four")"#, o!(4)),
        (r#"len("hello world")"#, o!(11)),
        (
            r#"len(1)"#,
            o!("argument to `len` not supported, got INTEGER"),
        ),
        (
            r#"len("one", "two")"#,
            o!("wrong number of arguments. got=2, want=1"),
        ),
    ] {
        let evaluated = test_eval(text);
        match expected {
            TestObject::Int(i) => test_integer_object(evaluated, i),
            TestObject::Ident(e) => test_error(evaluated, e),
            _ => unreachable!(),
        }
    }
}

#[test]
fn test_array_literal() {
    for (text, expected) in [
        ("[1, 2 * 2, 3 + 3]", vec![o!(1), o!(4), o!(6)]),
        ("[]", vec![]),
        ("[1]", vec![o!(1)]),
    ] {
        let evaluated = match test_eval(text) {
            Object::Array(a) => a,
            e => panic!("not an array: {}", e),
        };
        assert_eq!(evaluated.elements.len(), expected.len());
        for (act, exp) in evaluated.elements.into_iter().zip(expected.into_iter()) {
            test_object(act, exp);
        }
    }
}

#[test]
fn test_array_index_expressions() {
    for (test, expected) in [
        ("[1, 2, 3][0]", o!(1)),
        ("[1, 2, 3][1]", o!(2)),
        ("[1, 2, 3][2]", o!(3)),
        ("let i = 0; [1][i]", o!(1)),
        ("[1, 2, 3][1 + 1]", o!(3)),
        ("let myArray = [1, 2, 3]; myArray[2];", o!(3)),
        (
            "let myArray = [1, 2, 3]; myArray[0] + myArray[1] + myArray[2];",
            o!(6),
        ),
        (
            "let myArray = [1, 2, 3]; let i = myArray[0]; myArray[i];",
            o!(2),
        ),
        ("[1, 2, 3][3]", o!(null)),
        ("[1, 2, 3][-1]", o!(null)),
    ] {
        test_object(test_eval(test), expected);
    }
}

fn test_error(actual: Object, expected_message: &str) {
    let actual = match actual {
        Object::Error(e) => e,
        _ => panic!("not an error: {}", actual),
    };
    assert_eq!(actual.message, expected_message);
}

fn test_object(actual: Object, expected: TestObject) {
    match expected {
        TestObject::Int(i) => test_integer_object(actual, i),
        TestObject::Bool(b) => test_boolean_object(actual, b),
        TestObject::Null => test_null_object(actual),
        _ => unreachable!(),
    }
}

fn test_null_object(actual: Object) {
    match actual {
        Object::Null(_) => {}
        _ => panic!("not null: {}", actual),
    }
}

fn test_boolean_object(actual: Object, expected: bool) {
    let actual = match actual {
        Object::Boolean(b) => b,
        _ => panic!("not a boolean: {}", actual),
    };
    assert_eq!(actual.value, expected);
}

fn test_integer_object(actual: Object, expected: i64) {
    let actual = match actual {
        Object::Integer(i) => i,
        o => panic!("not an integer: {}", o),
    };
    assert_eq!(actual.value, expected);
}
