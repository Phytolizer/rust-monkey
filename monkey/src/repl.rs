use std::io;
use std::io::BufRead;
use std::io::BufReader;
use std::io::Read;
use std::io::Write;

use crate::evaluator::eval;
use crate::lexer::Lexer;
use crate::object::Environment;
use crate::parser::Parser;

const PROMPT: &str = ">> ";

pub fn start(input: &mut dyn Read, output: &mut dyn Write) -> io::Result<()> {
    let mut reader = BufReader::new(input);
    let mut line = String::new();
    let mut env = Environment::new();

    loop {
        write!(output, "{}", PROMPT)?;
        output.flush()?;

        line.clear();
        if reader.read_line(&mut line)? == 0 {
            break;
        }

        let l = Lexer::new(&line);
        let mut p = Parser::new(l);
        let program = p.parse_program();

        if !p.errors().is_empty() {
            print_parser_errors(output, p.errors())?;
            continue;
        }

        let result = eval(program.into(), &mut env);
        if !result.is_null() {
            writeln!(output, "{}", result)?;
        }
    }
    Ok(())
}

fn print_parser_errors(output: &mut dyn Write, errors: &[String]) -> io::Result<()> {
    for msg in errors {
        writeln!(output, "\t{}", msg)?;
    }
    Ok(())
}
