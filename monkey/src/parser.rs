use crate::ast::ArrayExpression;
use crate::ast::BlockStatement;
use crate::ast::BooleanExpression;
use crate::ast::CallExpression;
use crate::ast::Expression;
use crate::ast::ExpressionStatement;
use crate::ast::FunctionExpression;
use crate::ast::IdentifierExpression;
use crate::ast::IfExpression;
use crate::ast::IndexExpression;
use crate::ast::InfixExpression;
use crate::ast::IntegerLiteralExpression;
use crate::ast::LetStatement;
use crate::ast::PrefixExpression;
use crate::ast::Program;
use crate::ast::ReturnStatement;
use crate::ast::Statement;
use crate::ast::StringExpression;
use crate::lexer::Lexer;
use crate::token;
use crate::token::Token;
use crate::token::TokenType;

#[derive(Clone, Copy, PartialEq, Eq, PartialOrd, Ord)]
enum Precedence {
    Lowest,
    Equals,
    LessGreater,
    Sum,
    Product,
    Prefix,
    Call,
    Index,
}

enum PrefixDispatcher {
    Boolean,
    Function,
    Grouped,
    Identifier,
    If,
    IntegerLiteral,
    Prefix,
    String,
    Array,
}

enum InfixDispatcher {
    Infix,
    Call,
    Index,
}

pub struct Parser<'input> {
    l: Lexer<'input>,
    cur_token: Token,
    peek_token: Token,
    errors: Vec<String>,
}

impl<'input> Parser<'input> {
    pub fn new(l: Lexer<'input>) -> Self {
        let mut p = Self {
            l,
            // will be overwritten by the next two lines
            cur_token: Token::default(),
            peek_token: Token::default(),
            errors: Vec::new(),
        };

        p.next_token();
        p.next_token();

        p
    }

    pub fn errors(&self) -> &[String] {
        &self.errors
    }

    fn peek_error(&mut self, t: TokenType) {
        let msg = format!(
            "expected next token to be {}, got {} instead",
            t, self.peek_token.ty
        );
        self.errors.push(msg);
    }

    fn no_prefix_parse_fn_error(&mut self, t: TokenType) {
        let msg = format!("no prefix parse function for {} found", t);
        self.errors.push(msg);
    }

    fn next_token(&mut self) {
        std::mem::swap(&mut self.cur_token, &mut self.peek_token);
        self.peek_token = self.l.next_token();
    }

    pub fn parse_program(&mut self) -> Program {
        let mut statements = Vec::new();

        while self.cur_token.ty != token::EOF {
            if let Some(stmt) = self.parse_statement() {
                statements.push(stmt);
            }
            self.next_token();
        }

        Program { statements }
    }

    fn parse_statement(&mut self) -> Option<Statement> {
        match self.cur_token.ty {
            token::LET => self.parse_let_statement().map(Statement::from),
            token::RETURN => self.parse_return_statement().map(Statement::from),
            _ => self.parse_expression_statement().map(Statement::from),
        }
    }

    fn parse_let_statement(&mut self) -> Option<LetStatement> {
        let token = self.cur_token.clone();

        if !self.expect_peek(token::IDENT) {
            return None;
        }

        let name = IdentifierExpression {
            token: self.cur_token.clone(),
            value: self.cur_token.literal.clone(),
        };

        if !self.expect_peek(token::ASSIGN) {
            return None;
        }

        self.next_token();
        let value = self.parse_expression(Precedence::Lowest)?;

        if self.peek_token_is(token::SEMICOLON) {
            self.next_token();
        }

        Some(LetStatement { token, name, value })
    }

    fn expect_peek(&mut self, t: TokenType) -> bool {
        if self.peek_token_is(t) {
            self.next_token();
            true
        } else {
            self.peek_error(t);
            false
        }
    }

    fn cur_token_is(&self, t: TokenType) -> bool {
        self.cur_token.ty == t
    }

    fn peek_token_is(&self, t: TokenType) -> bool {
        self.peek_token.ty == t
    }

    fn parse_return_statement(&mut self) -> Option<ReturnStatement> {
        let token = self.cur_token.clone();

        self.next_token();

        let return_value = self.parse_expression(Precedence::Lowest)?;

        if self.peek_token_is(token::SEMICOLON) {
            self.next_token();
        }

        Some(ReturnStatement {
            token,
            return_value,
        })
    }

    fn parse_expression_statement(&mut self) -> Option<ExpressionStatement> {
        let token = self.cur_token.clone();

        let expression = self.parse_expression(Precedence::Lowest)?;

        if self.peek_token_is(token::SEMICOLON) {
            self.next_token();
        }

        Some(ExpressionStatement { token, expression })
    }

    fn parse_expression(&mut self, precedence: Precedence) -> Option<Expression> {
        let prefix = match prefix_dispatcher(self.cur_token.ty) {
            Some(p) => p,
            None => {
                self.no_prefix_parse_fn_error(self.cur_token.ty);
                return None;
            }
        };

        let mut left_exp = self.dispatch_prefix(prefix)?;

        while !self.peek_token_is(token::SEMICOLON) && precedence < self.peek_precedence() {
            let infix = match infix_dispatcher(self.peek_token.ty) {
                Some(i) => i,
                None => return Some(left_exp),
            };

            self.next_token();
            left_exp = self.dispatch_infix(Box::new(left_exp), infix)?;
        }

        Some(left_exp)
    }

    fn dispatch_prefix(&mut self, prefix: PrefixDispatcher) -> Option<Expression> {
        match prefix {
            PrefixDispatcher::Boolean => self.parse_boolean(),
            PrefixDispatcher::Function => self.parse_function_expression(),
            PrefixDispatcher::Grouped => self.parse_grouped_expression(),
            PrefixDispatcher::If => self.parse_if_expression(),
            PrefixDispatcher::Identifier => self.parse_identifier(),
            PrefixDispatcher::IntegerLiteral => self.parse_integer_literal(),
            PrefixDispatcher::Prefix => self.parse_prefix_expression(),
            PrefixDispatcher::String => self.parse_string_expression(),
            PrefixDispatcher::Array => self.parse_array_literal(),
        }
    }

    fn dispatch_infix(
        &mut self,
        left: Box<Expression>,
        infix: InfixDispatcher,
    ) -> Option<Expression> {
        match infix {
            InfixDispatcher::Infix => self.parse_infix_expression(left),
            InfixDispatcher::Call => self.parse_call_expression(left),
            InfixDispatcher::Index => self.parse_index_expression(left),
        }
    }

    fn parse_identifier(&mut self) -> Option<Expression> {
        Some(
            IdentifierExpression {
                token: self.cur_token.clone(),
                value: self.cur_token.literal.clone(),
            }
            .into(),
        )
    }

    fn parse_integer_literal(&mut self) -> Option<Expression> {
        let token = self.cur_token.clone();
        let value = match token.literal.parse::<i64>() {
            Ok(v) => v,
            Err(_) => {
                let msg = format!(r#"could not parse "{}" as integer"#, token.literal);
                self.errors.push(msg);
                return None;
            }
        };
        Some(Expression::IntegerLiteral(IntegerLiteralExpression {
            token,
            value,
        }))
    }

    fn parse_prefix_expression(&mut self) -> Option<Expression> {
        let token = self.cur_token.clone();
        let operator = self.cur_token.literal.clone();

        self.next_token();

        let right = self.parse_expression(Precedence::Prefix)?;
        let right = Box::new(right);

        Some(
            PrefixExpression {
                token,
                operator,
                right,
            }
            .into(),
        )
    }

    fn peek_precedence(&self) -> Precedence {
        precedence(self.peek_token.ty)
    }

    fn cur_precedence(&self) -> Precedence {
        precedence(self.cur_token.ty)
    }

    fn parse_infix_expression(&mut self, left: Box<Expression>) -> Option<Expression> {
        let token = self.cur_token.clone();
        let operator = self.cur_token.literal.clone();

        let precedence = self.cur_precedence();
        self.next_token();
        let right = self.parse_expression(precedence)?;
        let right = Box::new(right);

        Some(
            InfixExpression {
                token,
                left,
                operator,
                right,
            }
            .into(),
        )
    }

    fn parse_boolean(&mut self) -> Option<Expression> {
        Some(
            BooleanExpression {
                token: self.cur_token.clone(),
                value: self.cur_token_is(token::TRUE),
            }
            .into(),
        )
    }

    fn parse_grouped_expression(&mut self) -> Option<Expression> {
        self.next_token();

        let exp = self.parse_expression(Precedence::Lowest)?;

        if !self.expect_peek(token::RPAREN) {
            return None;
        }

        Some(exp)
    }

    fn parse_if_expression(&mut self) -> Option<Expression> {
        let token = self.cur_token.clone();

        if !self.expect_peek(token::LPAREN) {
            return None;
        }

        self.next_token();
        let condition = self.parse_expression(Precedence::Lowest)?;
        let condition = Box::new(condition);

        if !self.expect_peek(token::RPAREN) {
            return None;
        }

        if !self.expect_peek(token::LBRACE) {
            return None;
        }

        let consequence = self.parse_block_statement();
        let consequence = Box::new(consequence);

        let alternative = if self.peek_token_is(token::ELSE) {
            self.next_token();
            if !self.expect_peek(token::LBRACE) {
                return None;
            }

            let alternative = self.parse_block_statement();
            Some(Box::new(alternative))
        } else {
            None
        };

        Some(
            IfExpression {
                token,
                condition,
                consequence,
                alternative,
            }
            .into(),
        )
    }

    fn parse_block_statement(&mut self) -> BlockStatement {
        let token = self.cur_token.clone();
        let mut statements = Vec::new();

        self.next_token();

        while ![token::RBRACE, token::EOF].contains(&self.cur_token.ty) {
            if let Some(stmt) = self.parse_statement() {
                statements.push(stmt);
            }
            self.next_token();
        }

        BlockStatement { token, statements }
    }

    fn parse_function_expression(&mut self) -> Option<Expression> {
        let token = self.cur_token.clone();

        if !self.expect_peek(token::LPAREN) {
            return None;
        }

        let parameters = self.parse_function_parameters()?;

        if !self.expect_peek(token::LBRACE) {
            return None;
        }

        let body = self.parse_block_statement();
        let body = Box::new(body);

        Some(
            FunctionExpression {
                token,
                parameters,
                body,
            }
            .into(),
        )
    }

    fn parse_function_parameters(&mut self) -> Option<Vec<IdentifierExpression>> {
        let mut identifiers = Vec::new();

        if self.peek_token_is(token::RPAREN) {
            self.next_token();
            return Some(identifiers);
        }

        self.next_token();

        identifiers.push(IdentifierExpression {
            token: self.cur_token.clone(),
            value: self.cur_token.literal.clone(),
        });

        while self.peek_token_is(token::COMMA) {
            self.next_token();
            self.next_token();
            identifiers.push(IdentifierExpression {
                token: self.cur_token.clone(),
                value: self.cur_token.literal.clone(),
            });
        }

        if !self.expect_peek(token::RPAREN) {
            return None;
        }

        Some(identifiers)
    }

    fn parse_call_expression(&mut self, function: Box<Expression>) -> Option<Expression> {
        let token = self.cur_token.clone();
        let arguments = self.parse_expression_list(token::RPAREN)?;
        Some(
            CallExpression {
                token,
                function,
                arguments,
            }
            .into(),
        )
    }

    fn parse_expression_list(&mut self, end: TokenType) -> Option<Vec<Expression>> {
        let mut list = Vec::new();

        if self.peek_token_is(end) {
            self.next_token();
            return Some(list);
        }

        self.next_token();
        list.push(self.parse_expression(Precedence::Lowest)?);

        while self.peek_token_is(token::COMMA) {
            self.next_token();
            self.next_token();
            list.push(self.parse_expression(Precedence::Lowest)?);
        }

        if !self.expect_peek(end) {
            return None;
        }

        Some(list)
    }

    fn parse_string_expression(&self) -> Option<Expression> {
        Some(
            StringExpression {
                token: self.cur_token.clone(),
                value: self.cur_token.literal.clone(),
            }
            .into(),
        )
    }

    fn parse_array_literal(&mut self) -> Option<Expression> {
        let token = self.cur_token.clone();

        let elements = self.parse_expression_list(token::RBRACKET)?;

        Some(ArrayExpression { token, elements }.into())
    }

    fn parse_index_expression(&mut self, left: Box<Expression>) -> Option<Expression> {
        let token = self.cur_token.clone();

        self.next_token();

        let index = self.parse_expression(Precedence::Lowest)?;
        let index = Box::new(index);

        if !self.expect_peek(token::RBRACKET) {
            return None;
        }

        Some(IndexExpression { token, left, index }.into())
    }
}

fn precedence(ty: TokenType) -> Precedence {
    match ty {
        token::EQ => Precedence::Equals,
        token::NOT_EQ => Precedence::Equals,
        token::LT => Precedence::LessGreater,
        token::GT => Precedence::LessGreater,
        token::PLUS => Precedence::Sum,
        token::MINUS => Precedence::Sum,
        token::ASTERISK => Precedence::Product,
        token::SLASH => Precedence::Product,
        token::LPAREN => Precedence::Call,
        token::LBRACKET => Precedence::Index,
        _ => Precedence::Lowest,
    }
}

fn prefix_dispatcher(ty: TokenType) -> Option<PrefixDispatcher> {
    match ty {
        token::IDENT => Some(PrefixDispatcher::Identifier),
        token::INT => Some(PrefixDispatcher::IntegerLiteral),
        token::MINUS | token::BANG => Some(PrefixDispatcher::Prefix),
        token::TRUE | token::FALSE => Some(PrefixDispatcher::Boolean),
        token::LPAREN => Some(PrefixDispatcher::Grouped),
        token::IF => Some(PrefixDispatcher::If),
        token::FUNCTION => Some(PrefixDispatcher::Function),
        token::STRING => Some(PrefixDispatcher::String),
        token::LBRACKET => Some(PrefixDispatcher::Array),
        _ => None,
    }
}

fn infix_dispatcher(ty: TokenType) -> Option<InfixDispatcher> {
    match ty {
        token::PLUS
        | token::MINUS
        | token::ASTERISK
        | token::SLASH
        | token::LT
        | token::GT
        | token::EQ
        | token::NOT_EQ => Some(InfixDispatcher::Infix),
        token::LPAREN => Some(InfixDispatcher::Call),
        token::LBRACKET => Some(InfixDispatcher::Index),
        _ => None,
    }
}

#[cfg(test)]
mod tests;
