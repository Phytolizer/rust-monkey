use crate::lexer::Lexer;
use crate::token;
use crate::token::TokenType;

#[test]
fn test_next_token() {
    const INPUT: &str = r#"
        let five = 5;
        let ten = 10;

        let add = fn(x, y) {
            x + y;
        };

        let result = add(five, ten);
        !-/*5;
        5 < 10 > 5;

        if (5 < 10) {
            return true;
        } else {
            return false;
        }

        10 == 10;
        10 != 9;
        "foobar"
        "foo bar"
        [1, 2];
    "#;

    struct Test {
        expected_type: TokenType,
        expected_literal: &'static str,
    }

    impl Test {
        const fn new(expected_type: TokenType, expected_literal: &'static str) -> Self {
            Self {
                expected_type,
                expected_literal,
            }
        }
    }

    const TESTS: &[Test] = &[
        Test::new(token::LET, "let"),
        Test::new(token::IDENT, "five"),
        Test::new(token::ASSIGN, "="),
        Test::new(token::INT, "5"),
        Test::new(token::SEMICOLON, ";"),
        Test::new(token::LET, "let"),
        Test::new(token::IDENT, "ten"),
        Test::new(token::ASSIGN, "="),
        Test::new(token::INT, "10"),
        Test::new(token::SEMICOLON, ";"),
        Test::new(token::LET, "let"),
        Test::new(token::IDENT, "add"),
        Test::new(token::ASSIGN, "="),
        Test::new(token::FUNCTION, "fn"),
        Test::new(token::LPAREN, "("),
        Test::new(token::IDENT, "x"),
        Test::new(token::COMMA, ","),
        Test::new(token::IDENT, "y"),
        Test::new(token::RPAREN, ")"),
        Test::new(token::LBRACE, "{"),
        Test::new(token::IDENT, "x"),
        Test::new(token::PLUS, "+"),
        Test::new(token::IDENT, "y"),
        Test::new(token::SEMICOLON, ";"),
        Test::new(token::RBRACE, "}"),
        Test::new(token::SEMICOLON, ";"),
        Test::new(token::LET, "let"),
        Test::new(token::IDENT, "result"),
        Test::new(token::ASSIGN, "="),
        Test::new(token::IDENT, "add"),
        Test::new(token::LPAREN, "("),
        Test::new(token::IDENT, "five"),
        Test::new(token::COMMA, ","),
        Test::new(token::IDENT, "ten"),
        Test::new(token::RPAREN, ")"),
        Test::new(token::SEMICOLON, ";"),
        Test::new(token::BANG, "!"),
        Test::new(token::MINUS, "-"),
        Test::new(token::SLASH, "/"),
        Test::new(token::ASTERISK, "*"),
        Test::new(token::INT, "5"),
        Test::new(token::SEMICOLON, ";"),
        Test::new(token::INT, "5"),
        Test::new(token::LT, "<"),
        Test::new(token::INT, "10"),
        Test::new(token::GT, ">"),
        Test::new(token::INT, "5"),
        Test::new(token::SEMICOLON, ";"),
        Test::new(token::IF, "if"),
        Test::new(token::LPAREN, "("),
        Test::new(token::INT, "5"),
        Test::new(token::LT, "<"),
        Test::new(token::INT, "10"),
        Test::new(token::RPAREN, ")"),
        Test::new(token::LBRACE, "{"),
        Test::new(token::RETURN, "return"),
        Test::new(token::TRUE, "true"),
        Test::new(token::SEMICOLON, ";"),
        Test::new(token::RBRACE, "}"),
        Test::new(token::ELSE, "else"),
        Test::new(token::LBRACE, "{"),
        Test::new(token::RETURN, "return"),
        Test::new(token::FALSE, "false"),
        Test::new(token::SEMICOLON, ";"),
        Test::new(token::RBRACE, "}"),
        Test::new(token::INT, "10"),
        Test::new(token::EQ, "=="),
        Test::new(token::INT, "10"),
        Test::new(token::SEMICOLON, ";"),
        Test::new(token::INT, "10"),
        Test::new(token::NOT_EQ, "!="),
        Test::new(token::INT, "9"),
        Test::new(token::SEMICOLON, ";"),
        Test::new(token::STRING, "foobar"),
        Test::new(token::STRING, "foo bar"),
        Test::new(token::LBRACKET, "["),
        Test::new(token::INT, "1"),
        Test::new(token::COMMA, ","),
        Test::new(token::INT, "2"),
        Test::new(token::RBRACKET, "]"),
        Test::new(token::SEMICOLON, ";"),
        Test::new(token::EOF, ""),
    ];

    let mut l = Lexer::new(INPUT);
    for tt in TESTS {
        let tok = l.next_token();

        assert_eq!(tok.ty, tt.expected_type);
        assert_eq!(tok.literal, tt.expected_literal);
    }
}
