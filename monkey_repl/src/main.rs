use std::io::stdin;
use std::io::stdout;

use users::get_current_username;

fn main() {
    let username = get_current_username()
        .map(|un| un.to_string_lossy().to_string())
        .unwrap_or_else(|| String::from("anonymous"));

    println!(
        "Hello {}! This is the Monkey programming language!",
        username
    );
    println!("Feel free to type in commands");
    monkey::repl::start(&mut stdin(), &mut stdout()).unwrap();
}
